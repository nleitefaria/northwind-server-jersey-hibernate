package com.mycompany.myapp.rws;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.domain.CategoriesDTO;
import com.mycompany.myapp.service.CategoriesService;
import com.mycompany.myapp.service.impl.CategoriesServiceImpl;

@Path("/json/categories")
public class CategoriesRWS 
{
	private CategoriesService categoriesService;
	
	public CategoriesRWS()
	{	
		categoriesService = new CategoriesServiceImpl();	
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") Integer id) 
	{
		return Response.status(200).entity(categoriesService.findById(id)).build();
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll() 
	{
		return Response.status(200).entity(categoriesService.findAll()).build();
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(CategoriesDTO categoriesDTO) 
	{
		int id = categoriesService.persist(categoriesDTO);
		if(id > 0)
		{
			categoriesDTO.setId(id);
			return Response.status(201).entity(categoriesDTO).build();				
		}
		else
		{
			return Response.status(500).entity("An exception occurred").build();			
		}			
	}
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") Integer id, CategoriesDTO categoriesDTO) 
	{
		int ret = categoriesService.update(id, categoriesDTO);	         
	     
	    if(ret < 0)
	    {
	    	return Response.status(400).entity("Entity with id: " + id + "does not exist").build();	    	
	    }
	    
	    if(ret == 0)
	    {
	    	return Response.status(500).entity("An exception occurred").build();	    	
	    }
	    
	    return Response.status(200).entity("Successfully updated Entity with id: " + id).build();	    
	}
	
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") Integer id) 
	{
		int ret = categoriesService.delete(id);
		
		if(ret < 0)
	    {
	    	return Response.status(400).entity("Entity with id: " + id + "does not exist").build();	    	
	    }
	    
	    if(ret == 0)
	    {
	    	return Response.status(500).entity("An exception occurred").build();	    	
	    }
	    
	    return Response.status(200).entity("Successfully deleted Entity with id: " + id).build();
	}

}
