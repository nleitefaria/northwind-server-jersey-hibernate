package com.mycompany.myapp.rws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.service.OrdersService;
import com.mycompany.myapp.service.impl.OrdersServiceImpl;

@Path("/json/orders")
public class OrdersRWS {
	
	private OrdersService ordersService;
	
	public OrdersRWS()
	{	
		ordersService = new OrdersServiceImpl();	
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") Integer id) 
	{
		return Response.status(200).entity(ordersService.findById(id)).build();
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll() 
	{
		return Response.status(200).entity(ordersService.findAll()).build();
	}

}
