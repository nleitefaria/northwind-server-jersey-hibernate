package com.mycompany.myapp.rws;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.domain.OrderDetailsDTO;
import com.mycompany.myapp.service.OrderDetailsService;
import com.mycompany.myapp.service.impl.OrderDetailsServiceImpl;

@Path("/json/orderdetails")
public class OrderDetailsRWS 
{
	private OrderDetailsService orderDetailsService;
	
	public OrderDetailsRWS()
	{	
		orderDetailsService = new OrderDetailsServiceImpl();	
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") Integer id) 
	{
		return Response.status(200).entity(orderDetailsService.findById(id)).build();
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll() 
	{
		return Response.status(200).entity(orderDetailsService.findAll()).build();
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(OrderDetailsDTO orderDetailsDTO) 
	{
		int id = orderDetailsService.persist(orderDetailsDTO);
		if(id > 0)
		{
			orderDetailsDTO.setOrderId(orderDetailsDTO.getOrderId());
			return Response.status(201).entity(orderDetailsDTO).build();				
		}
		else
		{
			return Response.status(500).entity("An exception occurred").build();			
		}			
	}
	
//	@PUT
//	@Path("/{id}")
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response update(@PathParam("id") Integer id, OrderDetailsDTO orderDetailsDTO) 
//	{
//		int ret = orderDetailsService.update(id, orderDetailsDTO);	         
//	     
//	    if(ret < 0)
//	    {
//	    	return Response.status(400).entity("Entity with id: " + id + "does not exist").build();	    	
//	    }
//	    
//	    if(ret == 0)
//	    {
//	    	return Response.status(500).entity("An exception occurred").build();	    	
//	    }
//	    
//	    return Response.status(200).entity("Successfully updated Entity with id: " + id).build();	    
//	}



}
