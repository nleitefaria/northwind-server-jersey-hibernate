package com.mycompany.myapp.rws;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.domain.CustomerdemographicsDTO;
import com.mycompany.myapp.service.CustomerdemographicsService;
import com.mycompany.myapp.service.impl.CustomerdemographicsServiceImpl;

@Path("/json/customerdemographics")
public class CustomerdemographicsRWS 
{
	private CustomerdemographicsService customerdemographicsService;
	
	public CustomerdemographicsRWS()
	{	
		customerdemographicsService = new CustomerdemographicsServiceImpl();	
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") String id) 
	{
		return Response.status(200).entity(customerdemographicsService.findById(id)).build();
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll() 
	{
		return Response.status(200).entity(customerdemographicsService.findAll()).build();
	}
	
	
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(CustomerdemographicsDTO customerdemographicsDTO) 
	{
		int id = customerdemographicsService.persist(customerdemographicsDTO);
		if(id > 0)
		{
			return Response.status(201).entity(customerdemographicsDTO).build();				
		}
		else
		{
			return Response.status(500).entity("An exception occurred").build();			
		}			
	}
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") String id, CustomerdemographicsDTO customerdemographicsDTO) 
	{
		int ret = customerdemographicsService.update(id, customerdemographicsDTO);	         
	     
	    if(ret < 0)
	    {
	    	return Response.status(400).entity("Entity with id: " + id + "does not exist").build();	    	
	    }
	    
	    if(ret == 0)
	    {
	    	return Response.status(500).entity("An exception occurred").build();	    	
	    }
	    
	    return Response.status(200).entity("Successfully updated Entity with id: " + id).build();	    
	}


}
