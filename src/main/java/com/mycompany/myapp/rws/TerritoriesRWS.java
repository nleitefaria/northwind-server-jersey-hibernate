package com.mycompany.myapp.rws;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.domain.TerritoriesDTO;
import com.mycompany.myapp.service.TerritoriesService;
import com.mycompany.myapp.service.impl.TerritoriesServiceImpl;

@Path("/json/territories")
public class TerritoriesRWS 
{	
	private TerritoriesService territoriesService;
	
	public TerritoriesRWS()
	{	
		territoriesService = new TerritoriesServiceImpl();	
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") String id) 
	{
		return Response.status(200).entity(territoriesService.findById(id)).build();
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll() 
	{
		return Response.status(200).entity(territoriesService.findAll()).build();
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(TerritoriesDTO territoriesDTO) 
	{
		int id = territoriesService.persist(territoriesDTO);
		if(id > 0)
		{
			return Response.status(201).entity(territoriesDTO).build();				
		}
		else
		{
			return Response.status(500).entity("An exception occurred").build();			
		}			
	}
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") String id, TerritoriesDTO territoriesDTO) 
	{
		int ret = territoriesService.update(id, territoriesDTO);	         
	     
	    if(ret < 0)
	    {
	    	return Response.status(400).entity("Entity with id: " + id + "does not exist").build();	    	
	    }
	    
	    if(ret == 0)
	    {
	    	return Response.status(500).entity("An exception occurred").build();	    	
	    }
	    
	    return Response.status(200).entity("Successfully updated Entity with id: " + id).build();	    
	}
}
