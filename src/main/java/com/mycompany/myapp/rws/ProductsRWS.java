package com.mycompany.myapp.rws;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.domain.ProductsDTO;
import com.mycompany.myapp.service.ProductsService;
import com.mycompany.myapp.service.impl.ProductsServiceImpl;

@Path("/json/products")
public class ProductsRWS
{
	private ProductsService productsService;
	
	public ProductsRWS()
	{	
		productsService = new ProductsServiceImpl();	
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") Integer id) 
	{
		return Response.status(200).entity(productsService.findById(id)).build();
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll() 
	{
		return Response.status(200).entity(productsService.findAll()).build();
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(ProductsDTO productsDTO) 
	{
		int id = productsService.persist(productsDTO);
		if(id > 0)
		{
			productsDTO.setProductId(id);
			return Response.status(201).entity(productsDTO).build();				
		}
		else
		{
			return Response.status(500).entity("An exception occurred").build();			
		}			
	}
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") Integer id, ProductsDTO productsDTO) 
	{
		int ret = productsService.update(id, productsDTO);	         
	     
	    if(ret < 0)
	    {
	    	return Response.status(400).entity("Entity with id: " + id + "does not exist").build();	    	
	    }
	    
	    if(ret == 0)
	    {
	    	return Response.status(500).entity("An exception occurred").build();	    	
	    }
	    
	    return Response.status(200).entity("Successfully updated Entity with id: " + id).build();	    
	}

}
