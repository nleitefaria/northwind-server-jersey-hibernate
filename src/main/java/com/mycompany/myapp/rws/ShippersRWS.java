package com.mycompany.myapp.rws;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.domain.ShippersDTO;
import com.mycompany.myapp.service.ShippersService;
import com.mycompany.myapp.service.impl.ShippersServiceImpl;

@Path("/json/shippers")
public class ShippersRWS 
{
	private ShippersService shippersService;
	
	public ShippersRWS()
	{	
		shippersService = new ShippersServiceImpl();	
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") Integer id) 
	{
		return Response.status(200).entity(shippersService.findById(id)).build();
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll() 
	{
		return Response.status(200).entity(shippersService.findAll()).build();
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(ShippersDTO shippersDTO) 
	{
		int id = shippersService.persist(shippersDTO);
		if(id > 0)
		{
			shippersDTO.setShipperId(id);
			return Response.status(201).entity(shippersDTO).build();				
		}
		else
		{
			return Response.status(500).entity("An exception occurred").build();			
		}			
	}
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") Integer id, ShippersDTO shippersDTO) 
	{
		int ret = shippersService.update(id, shippersDTO);	         
	     
	    if(ret < 0)
	    {
	    	return Response.status(400).entity("Entity with id: " + id + "does not exist").build();	    	
	    }
	    
	    if(ret == 0)
	    {
	    	return Response.status(500).entity("An exception occurred").build();	    	
	    }
	    
	    return Response.status(200).entity("Successfully updated Entity with id: " + id).build();	    
	}
	

}
