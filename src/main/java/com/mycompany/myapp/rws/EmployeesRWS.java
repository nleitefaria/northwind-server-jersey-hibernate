package com.mycompany.myapp.rws;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.domain.EmployeesDTO;
import com.mycompany.myapp.service.EmployeesService;
import com.mycompany.myapp.service.impl.EmployeesServiceImpl;

@Path("/json/employees")
public class EmployeesRWS 
{
	private EmployeesService employeesService;
	
	public EmployeesRWS()
	{	
		employeesService = new EmployeesServiceImpl();	
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") Integer id) 
	{
		return Response.status(200).entity(employeesService.findById(id)).build();
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll() 
	{
		return Response.status(200).entity(employeesService.findAll()).build();
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(EmployeesDTO employeesDTO) 
	{
		int id = employeesService.persist(employeesDTO);
		if(id > 0)
		{
			employeesDTO.setEmployeeId(id);
			return Response.status(201).entity(employeesDTO).build();				
		}
		else
		{
			return Response.status(500).entity("An exception occurred").build();			
		}			
	}
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") Integer id, EmployeesDTO employeesDTO) 
	{
		int ret = employeesService.update(id, employeesDTO);	         
	     
	    if(ret < 0)
	    {
	    	return Response.status(400).entity("Entity with id: " + id + "does not exist").build();	    	
	    }
	    
	    if(ret == 0)
	    {
	    	return Response.status(500).entity("An exception occurred").build();	    	
	    }
	    
	    return Response.status(200).entity("Successfully updated Entity with id: " + id).build();	    
	}


}
