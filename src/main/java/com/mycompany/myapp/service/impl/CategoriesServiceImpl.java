package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.dao.CategoriesDAO;
import com.mycompany.myapp.dao.impl.CategoriesDAOImpl;
import com.mycompany.myapp.domain.CategoriesDTO;
import com.mycompany.myapp.entity.Categories;
import com.mycompany.myapp.entity.Region;
import com.mycompany.myapp.service.CategoriesService;

public class CategoriesServiceImpl implements  CategoriesService
{
	private CategoriesDAO categoriesDAO; 
	
	public CategoriesServiceImpl()
	{
		categoriesDAO = new CategoriesDAOImpl();
	}
	
	public CategoriesDTO findById(Integer id)
	{
		return new CategoriesDTO(categoriesDAO.findById(id));		
	}
	
	public List<CategoriesDTO> findAll()
	{		
		List<CategoriesDTO> ret = new ArrayList<CategoriesDTO>();
		
		for(Categories category : categoriesDAO.findAll())
		{
			ret.add(new CategoriesDTO(category));			
		}
		
		return ret;	
	}
	
	public int persist(CategoriesDTO categoriesDTO)
	{
		Categories category = new Categories();
		categoriesDTO.toCategories(categoriesDTO, category);
		return categoriesDAO.persist(category);
	}
	
	public int update(int id, CategoriesDTO categoriesDTO)
	{
		Categories category = categoriesDAO.findById(id);
		
		if(category != null)
		{
			category = categoriesDTO.toCategories(categoriesDTO, category);			
			return categoriesDAO.update(category);			
		}
		else
		{
			return -1;
		}		
	}
	
	public int delete(int id)
	{
		Categories region = categoriesDAO.findById(id);
		
		if(region != null)
		{	
			return categoriesDAO.delete(region);			
		}
		else
		{
			return -1;
		}		
	}

}
