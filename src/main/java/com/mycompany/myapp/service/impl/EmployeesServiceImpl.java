package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.dao.EmployeesDAO;
import com.mycompany.myapp.dao.impl.EmployeesDAOImpl;
import com.mycompany.myapp.domain.EmployeesDTO;
import com.mycompany.myapp.entity.Employees;
import com.mycompany.myapp.service.EmployeesService;

public class EmployeesServiceImpl implements EmployeesService{
	
	private EmployeesDAO employeesDAO; 
	
	public EmployeesServiceImpl()
	{
		employeesDAO = new EmployeesDAOImpl();
	}
	
	public EmployeesDTO findById(Integer id)
	{
		return new EmployeesDTO(employeesDAO.findById(id));		
	}
	
	public List<EmployeesDTO> findAll()
	{		
		List<EmployeesDTO> ret = new ArrayList<EmployeesDTO>();
		
		for(Employees employees : employeesDAO.findAll())
		{
			ret.add(new EmployeesDTO(employees));			
		}
		
		return ret;	
	}
	
	public int persist(EmployeesDTO employeesDTO)
	{
		Employees employees = new Employees();
	    employeesDTO.toEmployees(employeesDTO, employees);
		return employeesDAO.persist(employees);
	}
	
	public int update(int id, EmployeesDTO employeesDTO)
	{
		Employees employees = employeesDAO.findById(id);
		
		if(employees != null)
		{
			employeesDTO.toEmployees(employeesDTO, employees);			
			return employeesDAO.update(employees);			
		}
		else
		{
			return -1;
		}		
	}

}
