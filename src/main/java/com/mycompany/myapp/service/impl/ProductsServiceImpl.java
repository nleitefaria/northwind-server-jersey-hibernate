package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.dao.CategoriesDAO;
import com.mycompany.myapp.dao.ProductsDAO;
import com.mycompany.myapp.dao.SuppliersDAO;
import com.mycompany.myapp.dao.impl.CategoriesDAOImpl;
import com.mycompany.myapp.dao.impl.ProductsDAOImpl;
import com.mycompany.myapp.dao.impl.SuppliersDAOImpl;
import com.mycompany.myapp.domain.ProductsDTO;
import com.mycompany.myapp.entity.Categories;
import com.mycompany.myapp.entity.Products;
import com.mycompany.myapp.entity.Suppliers;
import com.mycompany.myapp.service.ProductsService;

public class ProductsServiceImpl implements ProductsService
{	
	private ProductsDAO productsDAO; 
	private SuppliersDAO suppliersDAO; 
	private CategoriesDAO categoriesDAO; 
	
	public ProductsServiceImpl()
	{
		productsDAO = new ProductsDAOImpl();
		suppliersDAO = new SuppliersDAOImpl();
		categoriesDAO = new CategoriesDAOImpl();
	}
	
	public ProductsDTO findById(Integer id)
	{
		return new ProductsDTO(productsDAO.findById(id));		
	}
	
	public List<ProductsDTO> findAll()
	{		
		List<ProductsDTO> ret = new ArrayList<ProductsDTO>();
		
		for(Products product : productsDAO.findAll())
		{
			ret.add(new ProductsDTO(product));			
		}
		
		return ret;	
	}
	
	public int persist(ProductsDTO productsDTO)
	{
		Suppliers suppliers = suppliersDAO.findById(productsDTO.getSuppliers().getSupplierId());
		Categories category = categoriesDAO.findById(productsDTO.getCategories().getId());
		
		if(suppliers == null || category == null)
		{
			return -1;
		}
		
		Products products = new Products();
		productsDTO.toProducts(productsDTO, products, suppliers, category);
		
		return productsDAO.persist(products);		
	}
	
	public int update(int id, ProductsDTO productsDTO)
	{
		Products products = productsDAO.findById(id);
		
		Suppliers suppliers = suppliersDAO.findById(products.getSuppliers().getSupplierId());
		Categories category = categoriesDAO.findById(products.getCategories().getId());
		
		if(suppliers == null || category == null)
		{
			return -1;
		}
		
		if(products != null)
		{
			products = productsDTO.toProducts(productsDTO, products, suppliers, category);			
			return productsDAO.update(products);			
		}
		else
		{
			return -1;
		}		
	}

}
