package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.dao.OrderDetailsDAO;
import com.mycompany.myapp.dao.OrdersDAO;
import com.mycompany.myapp.dao.ProductsDAO;
import com.mycompany.myapp.dao.impl.OrderDetailsDAOImpl;
import com.mycompany.myapp.dao.impl.OrdersDAOImpl;
import com.mycompany.myapp.dao.impl.ProductsDAOImpl;
import com.mycompany.myapp.domain.OrderDetailsDTO;
import com.mycompany.myapp.entity.OrderDetails;
import com.mycompany.myapp.entity.Orders;
import com.mycompany.myapp.entity.Products;
import com.mycompany.myapp.service.OrderDetailsService;

public class OrderDetailsServiceImpl implements OrderDetailsService
{
	private OrderDetailsDAO orderDetailsDAO; 	
	private OrdersDAO ordersDAO; 	
	private ProductsDAO productsDAO; 
	
	public OrderDetailsServiceImpl()
	{
		orderDetailsDAO = new OrderDetailsDAOImpl();
		ordersDAO = new OrdersDAOImpl(); 
		productsDAO = new ProductsDAOImpl();
	}
	
	public List<OrderDetailsDTO> findById(int id)
	{		
		List<OrderDetailsDTO> ret = new ArrayList<OrderDetailsDTO>();
		
		for(OrderDetails orderDetails : orderDetailsDAO.findById(id))
		{
			ret.add(new OrderDetailsDTO(orderDetails));			
		}
		
		return ret;	
	}
	
	
	public List<OrderDetailsDTO> findAll()
	{		
		List<OrderDetailsDTO> ret = new ArrayList<OrderDetailsDTO>();
		
		for(OrderDetails orderDetails : orderDetailsDAO.findAll())
		{
			ret.add(new OrderDetailsDTO(orderDetails));			
		}
		
		return ret;	
	}
	
	public int persist(OrderDetailsDTO orderDetailsDTO)
	{
		Orders orders = ordersDAO.findById(orderDetailsDTO.getOrderId());
		Products products = productsDAO.findById(orderDetailsDTO.getProductId());
		
		if(orders == null || products == null)
		{
			return -1;
		}
		
		OrderDetails orderDetails = new OrderDetails();
		orderDetails.setOrders(orders);
		orderDetails.setProducts(products);
		orderDetails.setUnitPrice(orderDetailsDTO.getUnitPrice());
		orderDetails.setQuantity(orderDetailsDTO.getQuantity());
		orderDetails.setDiscount(orderDetailsDTO.getDiscount());
		
		return orderDetailsDAO.persist(orderDetails);	
	}

}
