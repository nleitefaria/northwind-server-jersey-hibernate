package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.dao.CustomerdemographicsDAO;
import com.mycompany.myapp.dao.impl.CustomerdemographicsDAOImpl;
import com.mycompany.myapp.domain.CustomerdemographicsDTO;
import com.mycompany.myapp.entity.Customerdemographics;
import com.mycompany.myapp.service.CustomerdemographicsService;

public class CustomerdemographicsServiceImpl implements CustomerdemographicsService
{	
	private CustomerdemographicsDAO customerdemographicsDAO; 
	
	public CustomerdemographicsServiceImpl()
	{
		customerdemographicsDAO = new CustomerdemographicsDAOImpl();
	}
	
	public CustomerdemographicsDTO findById(String id)
	{
		return new CustomerdemographicsDTO(customerdemographicsDAO.findById(id));		
	}
	
	public List<CustomerdemographicsDTO> findAll()
	{		
		List<CustomerdemographicsDTO> ret = new ArrayList<CustomerdemographicsDTO>();
		
		for(Customerdemographics customerdemographics : customerdemographicsDAO.findAll())
		{
			ret.add(new CustomerdemographicsDTO(customerdemographics));			
		}
		
		return ret;	
	}
	
	public int persist(CustomerdemographicsDTO customerdemographicsDTO)
	{
		Customerdemographics customerdemographics = new Customerdemographics();
		customerdemographicsDTO.toCustomerdemographics(customerdemographicsDTO, customerdemographics);
		return customerdemographicsDAO.persist(customerdemographics);
	}
	
	public int update(String id, CustomerdemographicsDTO customerdemographicsDTO)
	{
		Customerdemographics customerdemographics = customerdemographicsDAO.findById(id);
		
		if(customerdemographics != null)
		{
			customerdemographics = customerdemographicsDTO.toCustomerdemographics(customerdemographicsDTO, customerdemographics);			
			return customerdemographicsDAO.update(customerdemographics);			
		}
		else
		{
			return -1;
		}		
	}

}
