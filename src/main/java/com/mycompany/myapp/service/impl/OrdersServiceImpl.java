package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.dao.OrdersDAO;
import com.mycompany.myapp.dao.impl.OrdersDAOImpl;
import com.mycompany.myapp.domain.OrdersDTO;
import com.mycompany.myapp.entity.Orders;
import com.mycompany.myapp.service.OrdersService;

public class OrdersServiceImpl implements OrdersService
{
	
	private OrdersDAO ordersDAO; 
	
	public OrdersServiceImpl()
	{
		ordersDAO = new OrdersDAOImpl();
	}
	
	public OrdersDTO findById(Integer id)
	{
		return new OrdersDTO(ordersDAO.findById(id));		
	}
	
	public List<OrdersDTO> findAll()
	{		
		List<OrdersDTO> ret = new ArrayList<OrdersDTO>();
		
		for(Orders order : ordersDAO.findAll())
		{
			ret.add(new OrdersDTO(order));			
		}
		
		return ret;	
	}

}
