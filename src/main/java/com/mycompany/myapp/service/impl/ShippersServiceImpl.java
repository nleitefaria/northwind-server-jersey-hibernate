package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.dao.ShippersDAO;
import com.mycompany.myapp.dao.impl.ShippersDAOImpl;
import com.mycompany.myapp.domain.ShippersDTO;
import com.mycompany.myapp.entity.Shippers;
import com.mycompany.myapp.service.ShippersService;

public class ShippersServiceImpl implements ShippersService
{
	private ShippersDAO shippersDAO; 
	
	public ShippersServiceImpl()
	{
		shippersDAO = new ShippersDAOImpl();
	}
	
	public ShippersDTO findById(Integer id)
	{
		return new ShippersDTO(shippersDAO.findById(id));		
	}
	
	public List<ShippersDTO> findAll()
	{		
		List<ShippersDTO> ret = new ArrayList<ShippersDTO>();
		
		for(Shippers shippers : shippersDAO.findAll())
		{
			ret.add(new ShippersDTO(shippers));			
		}
		
		return ret;	
	}
	
	public int persist(ShippersDTO shippersDTO)
	{
		Shippers shippers = new Shippers();
		shippersDTO.toShippers(shippersDTO, shippers);
		return shippersDAO.persist(shippers);
	}
	
	public int update(int id, ShippersDTO shippersDTO)
	{
		Shippers shippers = shippersDAO.findById(id);
		
		if(shippers != null)
		{
			shippers = shippersDTO.toShippers(shippersDTO, shippers);			
			return shippersDAO.update(shippers);			
		}
		else
		{
			return -1;
		}		
	}

}
