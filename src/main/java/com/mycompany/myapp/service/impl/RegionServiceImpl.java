package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.dao.RegionDAO;
import com.mycompany.myapp.dao.impl.RegionDAOImpl;
import com.mycompany.myapp.domain.RegionDTO;
import com.mycompany.myapp.entity.Region;
import com.mycompany.myapp.service.RegionService;

public class RegionServiceImpl implements RegionService 
{
	private RegionDAO regionDAO; 
	
	public RegionServiceImpl()
	{
		regionDAO = new RegionDAOImpl();
	}
	
	public RegionDTO findById(Integer id)
	{
		return new RegionDTO(regionDAO.findById(id));		
	}
	
	public List<RegionDTO> findAll()
	{		
		List<RegionDTO> ret = new ArrayList<RegionDTO>();
		
		for(Region region : regionDAO.findAll())
		{
			ret.add(new RegionDTO(region));			
		}
		
		return ret;	
	}
	
	public int persist(RegionDTO regionDTO)
	{
		Region region = new Region();
		regionDTO.toRegion(regionDTO, region);
		return regionDAO.persist(region);
	}
	
	public int update(int id, RegionDTO regionDTO)
	{
		Region region = regionDAO.findById(id);
		
		if(region != null)
		{
			region = regionDTO.toRegion(regionDTO, region);			
			return regionDAO.update(region);			
		}
		else
		{
			return -1;
		}		
	}
	
	public int delete(int id)
	{
		Region region = regionDAO.findById(id);
		
		if(region != null)
		{	
			return regionDAO.delete(region);			
		}
		else
		{
			return -1;
		}		
	}

}
