package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.dao.CustomersDAO;
import com.mycompany.myapp.dao.impl.CustomersDAOImpl;
import com.mycompany.myapp.domain.CustomersDTO;
import com.mycompany.myapp.entity.Customers;
import com.mycompany.myapp.service.CustomersService;

public class CustomersServiceImpl implements CustomersService
{
	private CustomersDAO customersDAO; 
	
	public CustomersServiceImpl()
	{
		customersDAO = new CustomersDAOImpl();
	}
	
	public CustomersDTO findById(String id)
	{
		return new CustomersDTO(customersDAO.findById(id));		
	}
	
	public List<CustomersDTO> findAll()
	{		
		List<CustomersDTO> ret = new ArrayList<CustomersDTO>();
		
		for(Customers customers : customersDAO.findAll())
		{
			ret.add(new CustomersDTO(customers));			
		}
		
		return ret;	
	}
	
	public int persist(CustomersDTO customersDTO)
	{
		Customers customers = new Customers();
		customers.setCustomerId(customersDTO.getCustomerId());	
		customersDTO.toCustomers(customersDTO, customers);
		return customersDAO.persist(customers);		
		
	}
	
	public int update(String id, CustomersDTO customersDTO)
	{
		Customers customers = customersDAO.findById(id);
		
		if(customers != null)
		{
			customers = customersDTO.toCustomers(customersDTO, customers);			
			return customersDAO.update(customers);			
		}
		else
		{
			return -1;
		}		
	}

}
