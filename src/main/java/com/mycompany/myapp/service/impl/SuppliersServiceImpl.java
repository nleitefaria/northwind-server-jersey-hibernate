package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.dao.SuppliersDAO;
import com.mycompany.myapp.dao.impl.SuppliersDAOImpl;
import com.mycompany.myapp.domain.SuppliersDTO;
import com.mycompany.myapp.entity.Suppliers;
import com.mycompany.myapp.service.SuppliersService;

public class SuppliersServiceImpl implements SuppliersService 
{	
	private SuppliersDAO suppliersDAO; 
	
	public SuppliersServiceImpl()
	{
		suppliersDAO = new SuppliersDAOImpl();
	}
	
	public SuppliersDTO findById(Integer id)
	{
		return new SuppliersDTO(suppliersDAO.findById(id));		
	}
	
	public List<SuppliersDTO> findAll()
	{		
		List<SuppliersDTO> ret = new ArrayList<SuppliersDTO>();
		
		for(Suppliers suppliers : suppliersDAO.findAll())
		{
			ret.add(new SuppliersDTO(suppliers));			
		}
		
		return ret;	
	}
	
	public int persist(SuppliersDTO suppliersDTO)
	{
		Suppliers suppliers = new Suppliers();
		suppliersDTO.toSuppliers(suppliersDTO, suppliers);
		return suppliersDAO.persist(suppliers);
	}
	
	public int update(int id, SuppliersDTO suppliersDTO)
	{
		Suppliers suppliers = suppliersDAO.findById(id);
		
		if(suppliers != null)
		{
			suppliers = suppliersDTO.toSuppliers(suppliersDTO, suppliers);	
			return suppliersDAO.update(suppliers);			
		}
		else
		{
			return -1;
		}		
	}

}
