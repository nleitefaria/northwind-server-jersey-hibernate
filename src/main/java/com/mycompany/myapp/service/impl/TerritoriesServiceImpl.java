package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.dao.RegionDAO;
import com.mycompany.myapp.dao.TerritoriesDAO;
import com.mycompany.myapp.dao.impl.TerritoriesDAOImpl;
import com.mycompany.myapp.domain.TerritoriesDTO;
import com.mycompany.myapp.entity.Region;
import com.mycompany.myapp.entity.Territories;
import com.mycompany.myapp.service.TerritoriesService;

public class TerritoriesServiceImpl implements TerritoriesService 
{
	private TerritoriesDAO territoriesDAO; 
	private RegionDAO regionDAO; 
	
	public TerritoriesServiceImpl()
	{
		territoriesDAO = new TerritoriesDAOImpl();
	}
	
	public TerritoriesDTO findById(String id)
	{
		return new TerritoriesDTO(territoriesDAO.findById(id));		
	}
	
	public List<TerritoriesDTO> findAll()
	{		
		List<TerritoriesDTO> ret = new ArrayList<TerritoriesDTO>();
		
		for(Territories territory : territoriesDAO.findAll())
		{
			ret.add(new TerritoriesDTO(territory));			
		}
		
		return ret;	
	}
	
	public int persist(TerritoriesDTO territoriesDTO)
	{
		Territories territories = new Territories();
		Region region = regionDAO.findById(territoriesDTO.getRegion().getRegionId());
		if(region!=null)
		{
			territories.setRegion(region);
			territoriesDTO.toTerritories(territoriesDTO, territories);
			return territoriesDAO.persist(territories);		
		}
		else
		{
			return -1;
		}
		
	}
	
	public int update(String id, TerritoriesDTO territoriesDTO)
	{
		Territories territories = territoriesDAO.findById(id);
		
		if(territories != null)
		{		
			Region region = regionDAO.findById(territoriesDTO.getRegion().getRegionId());
			if(region!=null)
			{
				territories.setRegion(region);		
				territories = territoriesDTO.toTerritories(territoriesDTO, territories);			
				return territoriesDAO.update(territories);	
			}
			else
			{
				return -1;
			}
		}
		else
		{
			return -1;
		}		
	}
}
