package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.RegionDTO;

public interface RegionService 
{
	RegionDTO findById(Integer id);
	List<RegionDTO> findAll();
	int persist(RegionDTO regionDTO);
	int update(int id, RegionDTO regionDTO);
	int delete(int id);
}
