package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.CategoriesDTO;

public interface CategoriesService
{
	CategoriesDTO findById(Integer id);
	List<CategoriesDTO> findAll();
	int persist(CategoriesDTO categoriesDTO);
	int update(int id, CategoriesDTO categoriesDTO);
	int delete(int id);
}
