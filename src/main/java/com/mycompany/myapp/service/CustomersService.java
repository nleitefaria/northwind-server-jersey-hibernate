package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.CustomersDTO;

public interface CustomersService {
	
	CustomersDTO findById(String id);
	List<CustomersDTO> findAll();
	int persist(CustomersDTO customersDTO);
	int update(String id, CustomersDTO customersDTO);

}
