package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.EmployeesDTO;

public interface EmployeesService {
	
	EmployeesDTO findById(Integer id);
	List<EmployeesDTO> findAll();
	int persist(EmployeesDTO employeesDTO);
	int update(int id, EmployeesDTO employeesDTO);

}
