package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.OrderDetailsDTO;

public interface OrderDetailsService
{	
	List<OrderDetailsDTO> findById(int id);
	List<OrderDetailsDTO> findAll();
	int persist(OrderDetailsDTO orderDetailsDTO);
}
