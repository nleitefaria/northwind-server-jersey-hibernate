package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.SuppliersDTO;

public interface SuppliersService 
{
	SuppliersDTO findById(Integer id);
	List<SuppliersDTO> findAll();
	int persist(SuppliersDTO suppliersDTO);
	int update(int id, SuppliersDTO suppliersDTO);
}
