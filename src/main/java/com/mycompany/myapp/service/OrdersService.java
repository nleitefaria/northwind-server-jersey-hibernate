package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.OrdersDTO;

public interface OrdersService 
{
	OrdersDTO findById(Integer id);
	List<OrdersDTO> findAll();

}
