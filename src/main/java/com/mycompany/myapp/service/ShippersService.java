package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.ShippersDTO;

public interface ShippersService
{
	ShippersDTO findById(Integer id);
	List<ShippersDTO> findAll();
	int persist(ShippersDTO shippersDTO);
	int update(int id, ShippersDTO shippersDTO);
}
