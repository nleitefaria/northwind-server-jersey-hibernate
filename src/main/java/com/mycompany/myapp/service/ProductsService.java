package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.ProductsDTO;

public interface ProductsService 
{
	ProductsDTO findById(Integer id);
	List<ProductsDTO> findAll();
	int persist(ProductsDTO productsDTO);
	int update(int id, ProductsDTO productsDTO);
}
