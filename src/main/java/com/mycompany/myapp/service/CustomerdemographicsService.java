package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.CustomerdemographicsDTO;

public interface CustomerdemographicsService {
	
	CustomerdemographicsDTO findById(String id);
	List<CustomerdemographicsDTO> findAll();
	int persist(CustomerdemographicsDTO customerdemographicsDTO);
	int update(String id, CustomerdemographicsDTO customerdemographicsDTO);

}
