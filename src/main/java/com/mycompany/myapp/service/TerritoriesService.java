package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.TerritoriesDTO;

public interface TerritoriesService
{	
	TerritoriesDTO findById(String id);
	List<TerritoriesDTO> findAll();
	int persist(TerritoriesDTO territoriesDTO);
	int update(String id, TerritoriesDTO territoriesDTO);
}
