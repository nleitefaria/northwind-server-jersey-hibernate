package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.Categories;

public interface CategoriesDAO
{
	List<Categories> findAll();
	Categories findById(Integer id);
	int persist(Categories entity);
	int update(Categories entity);
	int delete(Categories entity);
}
