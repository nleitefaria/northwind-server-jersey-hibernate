package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.Customers;

public interface CustomersDAO 
{
	 Customers findById(String id);
	 List<Customers> findAll();
	 int persist(Customers entity);
	 int update(Customers entity);
}
