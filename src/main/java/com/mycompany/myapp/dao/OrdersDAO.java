package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.Orders;

public interface OrdersDAO {
	
	Orders findById(Integer id);
	List<Orders> findAll();

}
