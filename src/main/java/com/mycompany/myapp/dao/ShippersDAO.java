package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.Shippers;

public interface ShippersDAO {
	
	Shippers findById(Integer id);
	List<Shippers> findAll();
	int persist(Shippers entity);
	int update(Shippers entity);

}
