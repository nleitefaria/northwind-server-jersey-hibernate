package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.Products;

public interface ProductsDAO 
{
	Products findById(Integer id);
	List<Products> findAll();
	int persist(Products entity);
	int update(Products entity);
}
