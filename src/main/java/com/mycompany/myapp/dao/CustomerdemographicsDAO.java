package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.Customerdemographics;

public interface CustomerdemographicsDAO {
	
	Customerdemographics findById(String id);
	List<Customerdemographics> findAll();
	int persist(Customerdemographics entity);
	int update(Customerdemographics entity);

}
