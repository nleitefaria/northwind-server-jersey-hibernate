package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.OrderDetails;

public interface OrderDetailsDAO
{	
	List<OrderDetails> findById(Integer id);
	List<OrderDetails> findAll();
	int persist(OrderDetails entity);
	int update(OrderDetails entity);
}
