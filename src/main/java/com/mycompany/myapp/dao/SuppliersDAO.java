package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.Suppliers;

public interface SuppliersDAO 
{
	Suppliers findById(Integer id);
	List<Suppliers> findAll();
	int persist(Suppliers entity);
	int update(Suppliers entity);
}
