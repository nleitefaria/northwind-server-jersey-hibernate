package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.Territories;

public interface TerritoriesDAO
{
	Territories findById(String id);
	List<Territories> findAll();
	int persist(Territories entity);
	int update(Territories entity);
}
