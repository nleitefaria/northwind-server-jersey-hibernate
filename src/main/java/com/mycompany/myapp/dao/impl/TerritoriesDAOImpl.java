package com.mycompany.myapp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mycompany.myapp.dao.TerritoriesDAO;
import com.mycompany.myapp.entity.Territories;
import com.mycompany.myapp.util.HibernateUtil;

public class TerritoriesDAOImpl implements TerritoriesDAO {
	
	private HibernateUtil hibernateUtil;

	public TerritoriesDAOImpl() {
		hibernateUtil = new HibernateUtil();
	}

	public Territories findById(String id) 
	{
		hibernateUtil.openCurrentSession();
		Territories territory = (Territories) hibernateUtil.getCurrentSession().createQuery("from Territories t left join fetch t.region AS r WHERE t.territoryId =" + id).uniqueResult();
		hibernateUtil.closeCurrentSession();
		return territory;
	}

	@SuppressWarnings("unchecked")
	public List<Territories> findAll() {
		hibernateUtil.openCurrentSession();
		List<Territories> territories = (List<Territories>) hibernateUtil.getCurrentSession().createQuery("from Territories t left join fetch t.region AS r").list();
		hibernateUtil.closeCurrentSession();
		return territories;
	}
	
	public int persist(Territories entity)
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;
		try 
		{
			tx = sess.beginTransaction();
			hibernateUtil.getCurrentSession().save(entity);		
			tx.commit();
			ret = 1;
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;

		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}
	
	public int update(Territories entity) 
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;		
		try 
		{
			tx = sess.beginTransaction();
			hibernateUtil.getCurrentSession().update(entity);		
			tx.commit();
			ret = 1;
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;
		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}

}
