package com.mycompany.myapp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mycompany.myapp.dao.EmployeesDAO;
import com.mycompany.myapp.entity.Employees;
import com.mycompany.myapp.util.HibernateUtil;

public class EmployeesDAOImpl implements EmployeesDAO{
	
	private HibernateUtil hibernateUtil;

	public EmployeesDAOImpl() {
		hibernateUtil = new HibernateUtil();
	}

	public Employees findById(Integer id)
	{
		hibernateUtil.openCurrentSession();
		Employees employee = (Employees) hibernateUtil.getCurrentSession().get(Employees.class, id);
		hibernateUtil.closeCurrentSession();
		return employee;
	}

	@SuppressWarnings("unchecked")
	public List<Employees> findAll() {
		hibernateUtil.openCurrentSession();
		List<Employees> employees = (List<Employees>) hibernateUtil.getCurrentSession().createQuery("from Employees").list();
		hibernateUtil.closeCurrentSession();
		return employees;
	}

	public int persist(Employees entity) {
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;
		try 
		{
			tx = sess.beginTransaction();
			ret =  (Integer) hibernateUtil.getCurrentSession().save(entity);		
			tx.commit();
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;

		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}
	
	public int update(Employees entity) {
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;
		try 
		{
			tx = sess.beginTransaction();
			hibernateUtil.getCurrentSession().update(entity);		
			tx.commit();
			ret = 1;
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;

		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}

}
