package com.mycompany.myapp.dao.impl;

import java.util.List;

import com.mycompany.myapp.dao.OrdersDAO;
import com.mycompany.myapp.entity.Orders;
import com.mycompany.myapp.util.HibernateUtil;

public class OrdersDAOImpl implements OrdersDAO 
{
	private HibernateUtil hibernateUtil;

	public OrdersDAOImpl() 
	{
		hibernateUtil = new HibernateUtil();
	}

	public Orders findById(Integer id)
	{
		hibernateUtil.openCurrentSession();
		Orders order = (Orders) hibernateUtil.getCurrentSession().createQuery("from Orders AS o left join fetch o.customers AS c left join fetch o.shippers AS s WHERE o.id =" + id).uniqueResult();
		hibernateUtil.closeCurrentSession();
		return order;
	}

	@SuppressWarnings("unchecked")
	public List<Orders> findAll() {
		hibernateUtil.openCurrentSession();
		List<Orders> orders = (List<Orders>) hibernateUtil.getCurrentSession().createQuery("from Orders AS o left join fetch o.customers AS c left join fetch o.shippers AS s").list();
		hibernateUtil.closeCurrentSession();
		return orders;
	}

}
