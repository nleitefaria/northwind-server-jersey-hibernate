package com.mycompany.myapp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mycompany.myapp.dao.RegionDAO;
import com.mycompany.myapp.entity.Region;
import com.mycompany.myapp.util.HibernateUtil;

public class RegionDAOImpl implements RegionDAO
{	
	private HibernateUtil hibernateUtil;

	public RegionDAOImpl() {
		hibernateUtil = new HibernateUtil();
	}

	public Region findById(Integer id) 
	{
		hibernateUtil.openCurrentSession();
		Region region = (Region) hibernateUtil.getCurrentSession().get(Region.class, id);
		hibernateUtil.closeCurrentSession();
		return region;
	}

	@SuppressWarnings("unchecked")
	public List<Region> findAll() {
		hibernateUtil.openCurrentSession();
		List<Region> books = (List<Region>) hibernateUtil.getCurrentSession().createQuery("from Region").list();
		hibernateUtil.closeCurrentSession();
		return books;
	}
	
	public int persist(Region entity)
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;
		try 
		{
			tx = sess.beginTransaction();
			ret =  (Integer) hibernateUtil.getCurrentSession().save(entity);		
			tx.commit();
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;

		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}
	
	public int update(Region entity) 
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;		
		try 
		{
			tx = sess.beginTransaction();
			hibernateUtil.getCurrentSession().update(entity);		
			tx.commit();
			ret = 1;
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;
		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}
	
	public int delete(Region entity) 
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;		
		try 
		{
			tx = sess.beginTransaction();
			hibernateUtil.getCurrentSession().delete(entity);		
			tx.commit();
			ret = 1;
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;
		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}

}
