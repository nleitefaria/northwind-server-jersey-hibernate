package com.mycompany.myapp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mycompany.myapp.dao.ShippersDAO;
import com.mycompany.myapp.entity.Shippers;
import com.mycompany.myapp.util.HibernateUtil;

public class ShippersDAOImpl implements ShippersDAO 
{
	private HibernateUtil hibernateUtil;

	public ShippersDAOImpl() {
		hibernateUtil = new HibernateUtil();
	}

	public Shippers findById(Integer id) 
	{
		hibernateUtil.openCurrentSession();
		Shippers shippers = (Shippers) hibernateUtil.getCurrentSession().get(Shippers.class, id);
		hibernateUtil.closeCurrentSession();
		return shippers;
	}

	@SuppressWarnings("unchecked")
	public List<Shippers> findAll() {
		hibernateUtil.openCurrentSession();
		List<Shippers> shippers = (List<Shippers>) hibernateUtil.getCurrentSession().createQuery("from Shippers").list();
		hibernateUtil.closeCurrentSession();
		return shippers;
	}
	
	public int persist(Shippers entity)
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;
		try 
		{
			tx = sess.beginTransaction();
			ret =  (Integer) hibernateUtil.getCurrentSession().save(entity);		
			tx.commit();
			
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;

		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}
	
	public int update(Shippers entity) 
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;		
		try 
		{
			tx = sess.beginTransaction();
			hibernateUtil.getCurrentSession().update(entity);		
			tx.commit();
			ret = 1;
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;
		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}

}
