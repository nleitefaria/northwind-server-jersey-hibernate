package com.mycompany.myapp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mycompany.myapp.dao.CategoriesDAO;
import com.mycompany.myapp.entity.Categories;
import com.mycompany.myapp.entity.Region;
import com.mycompany.myapp.util.HibernateUtil;

public class CategoriesDAOImpl implements CategoriesDAO 
{
	private HibernateUtil hibernateUtil;

	public CategoriesDAOImpl() {
		hibernateUtil = new HibernateUtil();
	}

	public Categories findById(Integer id) {
		hibernateUtil.openCurrentSession();
		Categories category = (Categories) hibernateUtil.getCurrentSession().get(Categories.class, id);
		hibernateUtil.closeCurrentSession();
		return category;
	}

	@SuppressWarnings("unchecked")
	public List<Categories> findAll() {
		hibernateUtil.openCurrentSession();
		List<Categories> books = (List<Categories>) hibernateUtil.getCurrentSession().createQuery("from Categories").list();
		hibernateUtil.closeCurrentSession();
		return books;
	}

	public int persist(Categories entity)
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;
		try 
		{
			tx = sess.beginTransaction();
			ret =  (Integer) hibernateUtil.getCurrentSession().save(entity);		
			tx.commit();
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;

		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}
	
	public int update(Categories entity) 
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;		
		try 
		{
			tx = sess.beginTransaction();
			hibernateUtil.getCurrentSession().update(entity);		
			tx.commit();
			ret = 1;
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;
		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}
	
	public int delete(Categories entity) 
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;		
		try 
		{
			tx = sess.beginTransaction();
			hibernateUtil.getCurrentSession().delete(entity);		
			tx.commit();
			ret = 1;
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;
		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}


}
