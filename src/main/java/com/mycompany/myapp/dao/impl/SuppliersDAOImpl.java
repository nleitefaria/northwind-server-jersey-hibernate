package com.mycompany.myapp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mycompany.myapp.dao.SuppliersDAO;
import com.mycompany.myapp.entity.Shippers;
import com.mycompany.myapp.entity.Suppliers;
import com.mycompany.myapp.util.HibernateUtil;

public class SuppliersDAOImpl implements SuppliersDAO {
	
	private HibernateUtil hibernateUtil;

	public SuppliersDAOImpl() {
		hibernateUtil = new HibernateUtil();
	}

	public Suppliers findById(Integer id) 
	{
		hibernateUtil.openCurrentSession();
		Suppliers suppliers = (Suppliers) hibernateUtil.getCurrentSession().get(Suppliers.class, id);
		hibernateUtil.closeCurrentSession();
		return suppliers;
	}

	@SuppressWarnings("unchecked")
	public List<Suppliers> findAll() {
		hibernateUtil.openCurrentSession();
		List<Suppliers> suppliers = (List<Suppliers>) hibernateUtil.getCurrentSession().createQuery("from Suppliers").list();
		hibernateUtil.closeCurrentSession();
		return suppliers;
	}
	
	public int persist(Suppliers entity)
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;
		try 
		{
			tx = sess.beginTransaction();
			ret =  (Integer) hibernateUtil.getCurrentSession().save(entity);		
			tx.commit();
			
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;

		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}
	
	public int update(Suppliers entity) 
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;		
		try 
		{
			tx = sess.beginTransaction();
			hibernateUtil.getCurrentSession().update(entity);		
			tx.commit();
			ret = 1;
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;
		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}

}
