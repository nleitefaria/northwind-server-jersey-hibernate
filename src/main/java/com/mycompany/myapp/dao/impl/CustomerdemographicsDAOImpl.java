package com.mycompany.myapp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mycompany.myapp.dao.CustomerdemographicsDAO;
import com.mycompany.myapp.entity.Customerdemographics;
import com.mycompany.myapp.util.HibernateUtil;

public class CustomerdemographicsDAOImpl implements CustomerdemographicsDAO{
	
	private HibernateUtil hibernateUtil;

	public CustomerdemographicsDAOImpl() {
		hibernateUtil = new HibernateUtil();
	}

	public Customerdemographics findById(String id) 
	{
		hibernateUtil.openCurrentSession();
		Customerdemographics customerdemographics = (Customerdemographics) hibernateUtil.getCurrentSession().get(Customerdemographics.class, id);
		hibernateUtil.closeCurrentSession();
		return customerdemographics;
	}

	@SuppressWarnings("unchecked")
	public List<Customerdemographics> findAll() {
		hibernateUtil.openCurrentSession();
		List<Customerdemographics> customerdemographics = (List<Customerdemographics>) hibernateUtil.getCurrentSession().createQuery("from Customerdemographics").list();
		hibernateUtil.closeCurrentSession();
		return customerdemographics;
	}
	
	public int persist(Customerdemographics entity)
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;
		try 
		{
			tx = sess.beginTransaction();
			hibernateUtil.getCurrentSession().save(entity);		
			tx.commit();
			ret = 1;
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;

		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}
	
	public int update(Customerdemographics entity) 
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;		
		try 
		{
			tx = sess.beginTransaction();
			hibernateUtil.getCurrentSession().update(entity);		
			tx.commit();
			ret = 1;
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;
		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}


}
