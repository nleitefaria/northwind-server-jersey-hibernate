package com.mycompany.myapp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mycompany.myapp.dao.OrderDetailsDAO;
import com.mycompany.myapp.entity.OrderDetails;
import com.mycompany.myapp.util.HibernateUtil;

public class OrderDetailsDAOImpl implements OrderDetailsDAO
{
	private HibernateUtil hibernateUtil;

	public OrderDetailsDAOImpl() 
	{
		hibernateUtil = new HibernateUtil();
	}

	@SuppressWarnings("unchecked")
	public List<OrderDetails> findById(Integer id)
	{
		hibernateUtil.openCurrentSession();
		List<OrderDetails> orderDetails = (List<OrderDetails>) hibernateUtil.getCurrentSession().createQuery("from OrderDetails od left join fetch od.orders AS o left join fetch od.products AS p left join fetch od.id AS odid WHERE od.id.orderId =" + id).list();
		hibernateUtil.closeCurrentSession();
		return orderDetails;
	}

	@SuppressWarnings("unchecked")
	public List<OrderDetails> findAll() 
	{
		hibernateUtil.openCurrentSession();                                                                 
		List<OrderDetails> orderDetails = (List<OrderDetails>) hibernateUtil.getCurrentSession().createQuery("from OrderDetails od left join fetch od.orders AS o left join fetch od.products AS p left join fetch od.id AS odid").list();
		hibernateUtil.closeCurrentSession();
		return orderDetails;
	}

	public int persist(OrderDetails entity)
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;
		try 
		{
			tx = sess.beginTransaction();
			hibernateUtil.getCurrentSession().save(entity);		
			tx.commit();
			ret = 1;
		} 
		catch (Exception e) 
		{
			System.out.println("************************");
			System.out.println(e.getMessage());
			System.out.println("************************");
			
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;

		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}
	
	public int update(OrderDetails entity) 
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;		
		try 
		{
			tx = sess.beginTransaction();
			hibernateUtil.getCurrentSession().update(entity);		
			tx.commit();
			ret = 1;
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;
		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}
	

}
