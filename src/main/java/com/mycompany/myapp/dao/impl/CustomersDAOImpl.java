package com.mycompany.myapp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mycompany.myapp.dao.CustomersDAO;
import com.mycompany.myapp.entity.Customers;
import com.mycompany.myapp.util.HibernateUtil;

public class CustomersDAOImpl implements CustomersDAO{
	
	private HibernateUtil hibernateUtil;

	public CustomersDAOImpl() {
		hibernateUtil = new HibernateUtil();
	}

	public Customers findById(String id) 
	{
		hibernateUtil.openCurrentSession();
		Customers customers = (Customers) hibernateUtil.getCurrentSession().get(Customers.class, id);
		hibernateUtil.closeCurrentSession();
		return customers;
	}

	@SuppressWarnings("unchecked")
	public List<Customers> findAll() {
		hibernateUtil.openCurrentSession();
		List<Customers> customers = (List<Customers>) hibernateUtil.getCurrentSession().createQuery("from Customers").list();
		hibernateUtil.closeCurrentSession();
		return customers;
	}
	
	public int persist(Customers entity)
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;
		try 
		{
			tx = sess.beginTransaction();
			hibernateUtil.getCurrentSession().save(entity);		
			tx.commit();
			ret = 1;
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;

		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}
	
	public int update(Customers entity) 
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;		
		try 
		{
			tx = sess.beginTransaction();
			hibernateUtil.getCurrentSession().update(entity);		
			tx.commit();
			ret = 1;
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;
		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}

}
