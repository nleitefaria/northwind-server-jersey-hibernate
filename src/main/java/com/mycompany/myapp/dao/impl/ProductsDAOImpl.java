package com.mycompany.myapp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mycompany.myapp.dao.ProductsDAO;
import com.mycompany.myapp.entity.Products;
import com.mycompany.myapp.util.HibernateUtil;

public class ProductsDAOImpl implements ProductsDAO 
{
	private HibernateUtil hibernateUtil;

	public ProductsDAOImpl() 
	{
		hibernateUtil = new HibernateUtil();
	}

	public Products findById(Integer id)
	{
		hibernateUtil.openCurrentSession();
		Products product = (Products) hibernateUtil.getCurrentSession().createQuery("from Products AS p left join fetch p.categories AS c left join fetch p.suppliers AS s WHERE p.id =" + id).uniqueResult();
		hibernateUtil.closeCurrentSession();
		return product;
	}

	@SuppressWarnings("unchecked")
	public List<Products> findAll() {
		hibernateUtil.openCurrentSession();
		List<Products> products = (List<Products>) hibernateUtil.getCurrentSession().createQuery("from Products AS p left join fetch p.categories AS c left join fetch p.suppliers AS s").list();
		hibernateUtil.closeCurrentSession();
		return products;
	}
	
	public int persist(Products entity)
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;
		try 
		{
			tx = sess.beginTransaction();
			ret =  (Integer) hibernateUtil.getCurrentSession().save(entity);		
			tx.commit();
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;

		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}
	
	public int update(Products entity) 
	{
		int ret = 0;
		Session sess = hibernateUtil.openCurrentSession();
		Transaction tx = null;		
		try 
		{
			tx = sess.beginTransaction();
			hibernateUtil.getCurrentSession().update(entity);		
			tx.commit();
			ret = 1;
		} 
		catch (Exception e) 
		{
			if (tx != null) 
			{
				tx.rollback();
			}
			ret = 0;
		} 
		finally 
		{
			hibernateUtil.closeCurrentSession();
		}
		return ret;
	}


}
