package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.Region;

public interface RegionDAO 
{	
	Region findById(Integer id);
	List<Region> findAll();
	int persist(Region entity);
	int update(Region entity);
	int delete(Region entity);
}
