package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.Employees;

public interface EmployeesDAO {
	
	List<Employees> findAll();
	Employees findById(Integer id);
	int persist(Employees entity);
	int update(Employees entity);

}
