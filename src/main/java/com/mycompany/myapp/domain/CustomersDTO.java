package com.mycompany.myapp.domain;

import com.mycompany.myapp.entity.Customers;

public class CustomersDTO
{
	private String customerId;
    private String companyName;
    private String contactName;
    private String contactTitle;
    private String address;
    private String city;
    private String region;
    private String postalCode;
    private String country;
    private String phone;
    private String fax;
    
    public CustomersDTO()
	{	
	}
    
	public CustomersDTO(String customerId, String companyName, String contactName, String contactTitle, String address, String city, String region, String postalCode, String country, String phone, String fax)
	{	
		this.customerId = customerId;
		this.companyName = companyName;
		this.contactName = contactName;
		this.contactTitle = contactTitle;
		this.address = address;
		this.city = city;
		this.region = region;
		this.postalCode = postalCode;
		this.country = country;
		this.phone = phone;
		this.fax = fax;
	}
	
	public CustomersDTO(Customers customers)
	{	
		this.customerId = customers.getCustomerId();
		this.companyName = customers.getCompanyName();
		this.contactName = customers.getContactName();
		this.contactTitle = customers.getContactTitle();
		this.address = customers.getAddress();
		this.city = customers.getCity();
		this.region = customers.getRegion();
		this.postalCode = customers.getPostalCode();
		this.country = customers.getCountry();
		this.phone = customers.getPhone();
		this.fax = customers.getFax();
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactTitle() {
		return contactTitle;
	}

	public void setContactTitle(String contactTitle) {
		this.contactTitle = contactTitle;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}
	
	public Customers toCustomers(CustomersDTO customersDTO, Customers customers)
	{	
		if(customersDTO.getCompanyName() != null)
			customers.setCompanyName(customersDTO.getCompanyName());		
		if(customersDTO.getContactName() != null)
			customers.setContactName(customersDTO.getContactName());		
		if(customersDTO.getContactTitle() != null)
			customers.setContactTitle(customersDTO.getContactTitle());		
		if(customersDTO.getAddress() != null)
			customers.setAddress(customersDTO.getAddress());		
		if(customersDTO.getCity() != null)
			customers.setCity(customersDTO.getCity());		
		if(customersDTO.getRegion() != null)
			customers.setRegion(customersDTO.getRegion());	
		if(customersDTO.getPostalCode() != null)
			customers.setPostalCode(customersDTO.getPostalCode());	
		if(customersDTO.getCountry() != null)
			customers.setCountry(customersDTO.getCountry());	
		if(customersDTO.getPhone() != null)
			customers.setPhone(customersDTO.getPhone());		
		if(customersDTO.getFax() != null)
			customers.setFax(customersDTO.getFax());		
		return customers;	
	}
}
