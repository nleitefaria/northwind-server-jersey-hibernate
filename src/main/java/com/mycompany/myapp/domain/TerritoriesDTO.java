package com.mycompany.myapp.domain;

import com.mycompany.myapp.entity.Territories;

public class TerritoriesDTO
{
	private String territoryId;
    private String territoryDescription;
    private RegionDTO region;
    
    public TerritoriesDTO() 
	{
	}
    
	public TerritoriesDTO(String territoryId, String territoryDescription, RegionDTO region) 
	{
		this.territoryId = territoryId;
		this.territoryDescription = territoryDescription;
		this.region = region;
	}

	public TerritoriesDTO(Territories territories) 
	{
		this.territoryId = territories.getTerritoryId();
		this.territoryDescription = territories.getTerritoryDescription();
		this.region = new RegionDTO(territories.getRegion());
	}

	public String getTerritoryId()
	{
		return territoryId;
	}

	public void setTerritoryId(String territoryId) 
	{
		this.territoryId = territoryId;
	}

	public String getTerritoryDescription()
	{
		return territoryDescription;
	}

	public void setTerritoryDescription(String territoryDescription)
	{
		this.territoryDescription = territoryDescription;
	}

	public RegionDTO getRegion() 
	{
		return region;
	}

	public void setRegion(RegionDTO region) 
	{
		this.region = region;
	}	
	
	public Territories toTerritories(TerritoriesDTO territoriesDTO, Territories territories)
	{		
		territories.setTerritoryId(territoriesDTO.getTerritoryId());
		if(territoriesDTO.getTerritoryDescription() != null)
			territories.setTerritoryDescription(territoriesDTO.getTerritoryDescription());
		return territories;
	}
}
