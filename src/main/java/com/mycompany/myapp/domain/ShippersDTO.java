package com.mycompany.myapp.domain;

import com.mycompany.myapp.entity.Shippers;

public class ShippersDTO 
{	
	private Integer shipperId;
    private String companyName;
    private String phone;
    
    public ShippersDTO()
	{			
	}
    
	public ShippersDTO(Integer shipperId, String companyName, String phone)
	{		
		this.shipperId = shipperId;
		this.companyName = companyName;
		this.phone = phone;
	}
    
	public ShippersDTO(Shippers shippers)
	{		
		this.shipperId = shippers.getShipperId();
		this.companyName = shippers.getCompanyName();
		this.phone = shippers.getPhone();
	}

	public Integer getShipperId() {
		return shipperId;
	}

	public void setShipperId(Integer shipperId) {
		this.shipperId = shipperId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public Shippers toShippers(ShippersDTO shippersDTO, Shippers shippers)
	{
		if(shippersDTO.getCompanyName() != null)
			shippers.setCompanyName(shippersDTO.getCompanyName());	
		if(shippersDTO.getPhone() != null)
			shippers.setPhone(shippersDTO.getPhone());	
		return shippers;
	}
}
