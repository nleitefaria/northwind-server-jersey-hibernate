package com.mycompany.myapp.domain;

import com.mycompany.myapp.entity.Customerdemographics;

public class CustomerdemographicsDTO
{
	private String customerTypeId;
    private String customerDesc;
    
    public CustomerdemographicsDTO()
	{	
	}
    
	public CustomerdemographicsDTO(String customerTypeId, String customerDesc)
	{
		this.customerTypeId = customerTypeId;
		this.customerDesc = customerDesc;
	}
    
	public CustomerdemographicsDTO(Customerdemographics customerdemographics)
	{
		this.customerTypeId = customerdemographics.getCustomerTypeId();
		this.customerDesc = customerdemographics.getCustomerDesc();
	}

	public String getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(String customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	public String getCustomerDesc() {
		return customerDesc;
	}

	public void setCustomerDesc(String customerDesc) {
		this.customerDesc = customerDesc;
	}
	
	public Customerdemographics toCustomerdemographics(CustomerdemographicsDTO customerdemographicsDTO, Customerdemographics customerdemographics)
	{
		if(customerdemographicsDTO.getCustomerTypeId() != null)
			customerdemographics.setCustomerTypeId(customerdemographicsDTO.getCustomerTypeId());
		if(customerdemographicsDTO.getCustomerDesc() != null)
			customerdemographics.setCustomerDesc(customerdemographicsDTO.getCustomerDesc());
		return customerdemographics;
	}
}
