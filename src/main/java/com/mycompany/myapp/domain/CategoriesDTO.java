package com.mycompany.myapp.domain;

import com.mycompany.myapp.entity.Categories;

public class CategoriesDTO
{
	private Integer id;
    private String categoryName;
    private String description;
    private byte[] picture;
   
	public CategoriesDTO()
	{		
	}

	public CategoriesDTO(Integer id, String categoryName, String description, byte[] picture) 
	{
		this.id = id;
		this.categoryName = categoryName;
		this.description = description;
		this.picture = picture;
	}
	
	public CategoriesDTO(Categories categories) 
	{
		this.id = categories.getId();
		this.categoryName = categories.getCategoryName();
		this.description = categories.getDescription();
		this.picture = categories.getPicture();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte[] getPicture() {
		return picture;
	}

	public void setPicture(byte[] picture) {
		this.picture = picture;
	}
	
	public Categories toCategories(CategoriesDTO categoriesDTO, Categories category)
	{
		if(categoriesDTO.getCategoryName() != null)
			category.setCategoryName(categoriesDTO.getCategoryName());
		if(categoriesDTO.getDescription() != null)
			category.setDescription(categoriesDTO.getDescription());
		if(categoriesDTO.getPicture() != null)
			category.setPicture(categoriesDTO.getPicture());
		return category;
	}
}
