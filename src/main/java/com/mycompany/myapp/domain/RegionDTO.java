package com.mycompany.myapp.domain;

import com.mycompany.myapp.entity.Region;

public class RegionDTO {
	
	private int regionId;
    private String regionDescription;
    
    public RegionDTO() 
	{
	}
    
	public RegionDTO(int regionId, String regionDescription) 
	{
		this.regionId = regionId;
		this.regionDescription = regionDescription;
	}
    
	public RegionDTO(Region region) 
	{
		this.regionId = region.getRegionId();
		this.regionDescription = region.getRegionDescription();
	}

	public int getRegionId() {
		return regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

	public String getRegionDescription() {
		return regionDescription;
	}

	public void setRegionDescription(String regionDescription) {
		this.regionDescription = regionDescription;
	}
	
	public Region toRegion(RegionDTO regionDTO, Region region)
	{
		region.setRegionId(regionDTO.getRegionId());
		if(regionDTO.getRegionDescription() != null)
			region.setRegionDescription(regionDTO.getRegionDescription());
		return region;
	}
}
