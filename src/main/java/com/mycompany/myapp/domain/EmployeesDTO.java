package com.mycompany.myapp.domain;

import java.util.Date;

import com.mycompany.myapp.entity.Employees;

public class EmployeesDTO 
{
	private Integer employeeId;
    private String lastName;
    private String firstName;
    private String title;
    private String titleOfCourtesy;
    private Date birthDate;
    private Date hireDate;
    private String address;
    private String city;
    private String region;
    private String postalCode;
    private String country;
    private String homePhone;
    private String extension;
    private byte[] photo;
    private String notes;
    private String photoPath;
    private Float salary;  
    private ReportsToDTO reportsTo;
    

	public EmployeesDTO() {
		
	}

	public EmployeesDTO(Integer employeeId, String lastName, String firstName, String title, String titleOfCourtesy, Date birthDate, Date hireDate, String address, String city, String region, String postalCode, String country, String homePhone, String extension, byte[] photo, String notes, String photoPath, Float salary, ReportsToDTO reportsTo) 
	{	
		this.employeeId = employeeId;
		this.lastName = lastName;
		this.firstName = firstName;
		this.title = title;
		this.titleOfCourtesy = titleOfCourtesy;
		this.birthDate = birthDate;
		this.hireDate = hireDate;
		this.address = address;
		this.city = city;
		this.region = region;
		this.postalCode = postalCode;
		this.country = country;
		this.homePhone = homePhone;
		this.extension = extension;
		this.photo = photo;
		this.notes = notes;
		this.photoPath = photoPath;
		this.salary = salary;
		this.reportsTo = reportsTo;
	}
	
	public EmployeesDTO(Employees employees) 
	{	
		this.employeeId = employees.getEmployeeId();
		this.lastName = employees.getLastName();
		this.firstName = employees.getFirstName();
		this.title = employees.getTitle();
		this.titleOfCourtesy = employees.getTitleOfCourtesy();
		this.birthDate = employees.getBirthDate();
		this.hireDate = employees.getHireDate();
		this.address = employees.getAddress();
		this.city = employees.getCity();
		this.region = employees.getRegion();
		this.postalCode = employees.getPostalCode();
		this.country = employees.getCountry();
		this.homePhone = employees.getHomePhone();
		this.extension = employees.getExtension();
		this.photo = employees.getPhoto();
		this.notes = employees.getNotes();
		this.photoPath = employees.getPhotoPath();
		this.salary = employees.getSalary();
		if(employees.getEmployees() != null)
			this.reportsTo = new ReportsToDTO(employees.getEmployees().getEmployeeId(), employees.getEmployees().getLastName(), employees.getEmployees().getFirstName());
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleOfCourtesy() {
		return titleOfCourtesy;
	}

	public void setTitleOfCourtesy(String titleOfCourtesy) {
		this.titleOfCourtesy = titleOfCourtesy;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public Float getSalary() {
		return salary;
	}

	public void setSalary(Float salary) {
		this.salary = salary;
	}

	public ReportsToDTO getReportsTo() {
		return reportsTo;
	}

	public void setReportsTo(ReportsToDTO reportsTo) {
		this.reportsTo = reportsTo;
	}
	
	public void toEmployees(EmployeesDTO employeesDTO, Employees employees)
	{
		if(employeesDTO.getLastName() != null)
			employees.setLastName(employeesDTO.getLastName());
		if(employeesDTO.getLastName() != null)
			employees.setFirstName(employeesDTO.getFirstName()); 
		if(employeesDTO.getFirstName() != null)
			employees.setTitle(employeesDTO.getTitle());
		if(employeesDTO.getTitleOfCourtesy() != null)
			employees.setTitleOfCourtesy(employeesDTO.getTitleOfCourtesy()); 
		if(employeesDTO.getBirthDate() != null)
			employees.setBirthDate(employeesDTO.getBirthDate());
		if(employeesDTO.getHireDate() != null)
			employees.setHireDate(employeesDTO.getHireDate());
		if(employeesDTO.getAddress() != null)
			employees.setAddress(employeesDTO.getAddress());
		if(employeesDTO.getCity() != null)
			employees.setCity(employeesDTO.getCity());
		if(employeesDTO.getRegion() != null)
			employees.setRegion(employeesDTO.getRegion()); 
		if(employeesDTO.getPostalCode() != null)
			employees.setPostalCode(employeesDTO.getPostalCode()); 
		if(employeesDTO.getCountry() != null)
			employees.setCountry(employeesDTO.getCountry());
		if(employeesDTO.getHomePhone() != null)
			employees.setHomePhone(employeesDTO.getHomePhone()); 
		if(employeesDTO.getExtension() != null)
			employees.setExtension(employeesDTO.getExtension());
		if(employeesDTO.getPhoto() != null)
			employees.setPhoto(employeesDTO.getPhoto());
		if(employeesDTO.getNotes() != null)
			employees.setNotes(employeesDTO.getNotes());
		if(employeesDTO.getPhotoPath() != null)
			employees.setPhotoPath(employeesDTO.getPhotoPath()); 
		if(employeesDTO.getSalary() != null)
			employees.setSalary(employeesDTO.getSalary());
	}
}
