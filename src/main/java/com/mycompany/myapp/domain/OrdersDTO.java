package com.mycompany.myapp.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.mycompany.myapp.entity.Orders;

public class OrdersDTO 
{
	private Integer orderId;
    private CustomersDTO customers;
    private ShippersDTO shippers;
    private Date orderDate;
    private Date requiredDate;
    private Date shippedDate;
    private BigDecimal freight;
    private String shipName;
    private String shipAddress;
    private String shipCity;
    private String shipRegion;
    private String shipPostalCode;
    private String shipCountry;
     
	public OrdersDTO() {	
	}

	public OrdersDTO(Integer orderId, CustomersDTO customers, ShippersDTO shippers, Date orderDate, Date requiredDate, Date shippedDate, BigDecimal freight, String shipName, String shipAddress, String shipCity, String shipRegion, String shipPostalCode, String shipCountry) 
	{		
		this.orderId = orderId;
		this.customers = customers;
		this.shippers = shippers;
		this.orderDate = orderDate;
		this.requiredDate = requiredDate;
		this.shippedDate = shippedDate;
		this.freight = freight;
		this.shipName = shipName;
		this.shipAddress = shipAddress;
		this.shipCity = shipCity;
		this.shipRegion = shipRegion;
		this.shipPostalCode = shipPostalCode;
		this.shipCountry = shipCountry;
	}

	public OrdersDTO(Orders orders) 
	{		
		this.orderId = orders.getOrderId();
		this.customers = new CustomersDTO(orders.getCustomers());
		this.shippers = new ShippersDTO(orders.getShippers());
		this.orderDate = orders.getOrderDate();
		this.requiredDate = orders.getRequiredDate();
		this.shippedDate = orders.getShippedDate();
		this.freight = orders.getFreight();
		this.shipName = orders.getShipName();
		this.shipAddress = orders.getShipAddress();
		this.shipCity = orders.getShipCity();
		this.shipRegion = orders.getShipRegion();
		this.shipPostalCode = orders.getShipPostalCode();
		this.shipCountry = orders.getShipCountry();
	}



	public Integer getOrderId() {
		return orderId;
	}



	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}



	public CustomersDTO getCustomers() {
		return customers;
	}



	public void setCustomers(CustomersDTO customers) {
		this.customers = customers;
	}

	public ShippersDTO getShippers() {
		return shippers;
	}



	public void setShippers(ShippersDTO shippers) {
		this.shippers = shippers;
	}



	public Date getOrderDate() {
		return orderDate;
	}



	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}



	public Date getRequiredDate() {
		return requiredDate;
	}



	public void setRequiredDate(Date requiredDate) {
		this.requiredDate = requiredDate;
	}



	public Date getShippedDate() {
		return shippedDate;
	}



	public void setShippedDate(Date shippedDate) {
		this.shippedDate = shippedDate;
	}



	public BigDecimal getFreight() {
		return freight;
	}



	public void setFreight(BigDecimal freight) {
		this.freight = freight;
	}



	public String getShipName() {
		return shipName;
	}



	public void setShipName(String shipName) {
		this.shipName = shipName;
	}



	public String getShipAddress() {
		return shipAddress;
	}



	public void setShipAddress(String shipAddress) {
		this.shipAddress = shipAddress;
	}



	public String getShipCity() {
		return shipCity;
	}



	public void setShipCity(String shipCity) {
		this.shipCity = shipCity;
	}



	public String getShipRegion() {
		return shipRegion;
	}



	public void setShipRegion(String shipRegion) {
		this.shipRegion = shipRegion;
	}



	public String getShipPostalCode() {
		return shipPostalCode;
	}



	public void setShipPostalCode(String shipPostalCode) {
		this.shipPostalCode = shipPostalCode;
	}



	public String getShipCountry() {
		return shipCountry;
	}



	public void setShipCountry(String shipCountry) {
		this.shipCountry = shipCountry;
	}
	
	
    
    

}
