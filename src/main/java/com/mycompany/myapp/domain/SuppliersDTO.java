package com.mycompany.myapp.domain;

import com.mycompany.myapp.entity.Suppliers;
import com.mycompany.myapp.entity.Territories;

public class SuppliersDTO 
{
	private Integer supplierId;
    private String companyName;
    private String contactName;
    private String contactTitle;
    private String address;
    private String city;
    private String region;
    private String postalCode;
    private String country;
    private String phone;
    private String fax;
    private String homePage;
    
    public SuppliersDTO()
    {			
	}
    
	public SuppliersDTO(Integer supplierId, String companyName, String contactName, String contactTitle, String address, String city, String region, String postalCode, String country, String phone, String fax, String homePage)
	{	
		this.supplierId = supplierId;
		this.companyName = companyName;
		this.contactName = contactName;
		this.contactTitle = contactTitle;
		this.address = address;
		this.city = city;
		this.region = region;
		this.postalCode = postalCode;
		this.country = country;
		this.phone = phone;
		this.fax = fax;
		this.homePage = homePage;
	}
    
	public SuppliersDTO(Suppliers suppliers)
	{	
		this.supplierId = suppliers.getSupplierId();
		this.companyName = suppliers.getCompanyName();
		this.contactName = suppliers.getContactName();
		this.contactTitle = suppliers.getContactTitle();
		this.address = suppliers.getAddress();
		this.city = suppliers.getCity();
		this.region = suppliers.getRegion();
		this.postalCode = suppliers.getPostalCode();
		this.country = suppliers.getCountry();
		this.phone = suppliers.getPhone();
		this.fax = suppliers.getFax();
		this.homePage = suppliers.getHomePage();
	}

	public Integer getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactTitle() {
		return contactTitle;
	}

	public void setContactTitle(String contactTitle) {
		this.contactTitle = contactTitle;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getHomePage() {
		return homePage;
	}

	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}
	
	public Suppliers toSuppliers(SuppliersDTO suppliersDTO, Suppliers suppliers)
	{			
		if(suppliersDTO.getCompanyName() != null)
			suppliers.setCompanyName(suppliersDTO.getCompanyName());		
		if(suppliersDTO.getContactName() != null)
			suppliers.setContactName(suppliersDTO.getContactName());		
		if(suppliersDTO.getContactTitle() != null)
			suppliers.setContactTitle(suppliersDTO.getContactTitle());
		if(suppliersDTO.getAddress() != null)
			suppliers.setAddress(suppliersDTO.getAddress());	
		if(suppliersDTO.getCity() != null)
			suppliers.setCity(suppliersDTO.getCity());		
		if(suppliersDTO.getRegion() != null)
			suppliers.setRegion(suppliersDTO.getRegion());		
		if(suppliersDTO.getPostalCode() != null)
			suppliers.setPostalCode(suppliersDTO.getPostalCode());	
		if(suppliersDTO.getCountry() != null)
			suppliers.setCountry(suppliersDTO.getCountry());		
		if(suppliersDTO.getPhone() != null)
			suppliers.setPhone(suppliersDTO.getPhone());		
		if(suppliersDTO.getFax() != null)
			suppliers.setFax(suppliersDTO.getFax());		
		if(suppliersDTO.getHomePage() != null)
			suppliers.setHomePage(suppliersDTO.getHomePage());	
		return suppliers;
	}
}
