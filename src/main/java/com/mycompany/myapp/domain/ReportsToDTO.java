package com.mycompany.myapp.domain;

public class ReportsToDTO {
	
	private Integer reportsToEmployeeId;
    private String reportsToLastName;
    private String reportsToFirstName;
     
	public ReportsToDTO() {
		
	}

	public ReportsToDTO(Integer reportsToEmployeeId, String reportsToLastName, String reportsToFirstName) 
	{
		this.reportsToEmployeeId = reportsToEmployeeId;
		this.reportsToLastName = reportsToLastName;
		this.reportsToFirstName = reportsToFirstName;
	}

	public Integer getReportsToEmployeeId() {
		return reportsToEmployeeId;
	}

	public void setReportsToEmployeeId(Integer reportsToEmployeeId) {
		this.reportsToEmployeeId = reportsToEmployeeId;
	}

	public String getReportsToLastName() {
		return reportsToLastName;
	}

	public void setReportsToLastName(String reportsToLastName) {
		this.reportsToLastName = reportsToLastName;
	}

	public String getReportsToFirstName() {
		return reportsToFirstName;
	}

	public void setReportsToFirstName(String reportsToFirstName) {
		this.reportsToFirstName = reportsToFirstName;
	}

	
	
    
    

}
