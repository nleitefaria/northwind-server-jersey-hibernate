package com.mycompany.myapp.domain;

import java.math.BigDecimal;

import com.mycompany.myapp.entity.OrderDetails;

public class OrderDetailsDTO 
{
	private Integer orderId;	
	private Integer productId;
	private String productName;  
    private BigDecimal unitPrice;
    private short quantity;
    private double discount;
    
	public OrderDetailsDTO()
	{	
	}

	public OrderDetailsDTO(Integer orderId, Integer productId, String productName, BigDecimal unitPrice, short quantity, double discount) 
	{	
		this.orderId = orderId;
		this.productId = productId;
		this.productName = productName;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
		this.discount = discount;
	}

	public OrderDetailsDTO(OrderDetails orderDetails) 
	{	
		this.orderId = orderDetails.getId().getOrderId();
		this.productId = orderDetails.getProducts().getProductId();
		this.productName = orderDetails.getProducts().getProductName();
		this.unitPrice = orderDetails.getUnitPrice();
		this.quantity = orderDetails.getQuantity();
		this.discount = orderDetails.getDiscount();
	}
	
	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public short getQuantity() {
		return quantity;
	}

	public void setQuantity(short quantity) {
		this.quantity = quantity;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
	
	
	
}
