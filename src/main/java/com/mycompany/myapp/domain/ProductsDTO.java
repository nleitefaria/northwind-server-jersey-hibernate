package com.mycompany.myapp.domain;

import java.math.BigDecimal;
import java.util.Locale.Category;

import com.mycompany.myapp.entity.Categories;
import com.mycompany.myapp.entity.Products;
import com.mycompany.myapp.entity.Suppliers;

public class ProductsDTO
{
	private Integer productId;
    private CategoriesDTO categories;
    private SuppliersDTO suppliers;
    private String productName;
    private String quantityPerUnit;
    private BigDecimal unitPrice;
    private Short unitsInStock;
    private Short unitsOnOrder;
    private Short reorderLevel;
    private boolean discontinued;
      
	public ProductsDTO() 
	{		
	}

	public ProductsDTO(Integer productId, CategoriesDTO categories, SuppliersDTO suppliers, String productName, String quantityPerUnit, BigDecimal unitPrice, Short unitsInStock, Short unitsOnOrder, Short reorderLevel, boolean discontinued) 
	{
		this.productId = productId;
		this.categories = categories;
		this.suppliers = suppliers;
		this.productName = productName;
		this.quantityPerUnit = quantityPerUnit;
		this.unitPrice = unitPrice;
		this.unitsInStock = unitsInStock;
		this.unitsOnOrder = unitsOnOrder;
		this.reorderLevel = reorderLevel;
		this.discontinued = discontinued;
	}
	
	public ProductsDTO(Products products) 
	{
		this.productId = products.getProductId();
		this.categories = new CategoriesDTO(products.getCategories());
		this.suppliers = new SuppliersDTO(products.getSuppliers());
		this.productName = products.getProductName();
		this.quantityPerUnit = products.getQuantityPerUnit();
		this.unitPrice = products.getUnitPrice();
		this.unitsInStock = products.getUnitsInStock();
		this.unitsOnOrder = products.getUnitsOnOrder();
		this.reorderLevel = products.getReorderLevel();
		this.discontinued = products.isDiscontinued();
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public CategoriesDTO getCategories() {
		return categories;
	}

	public void setCategories(CategoriesDTO categories) {
		this.categories = categories;
	}

	public SuppliersDTO getSuppliers() {
		return suppliers;
	}

	public void setSuppliers(SuppliersDTO suppliers) {
		this.suppliers = suppliers;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getQuantityPerUnit() {
		return quantityPerUnit;
	}

	public void setQuantityPerUnit(String quantityPerUnit) {
		this.quantityPerUnit = quantityPerUnit;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Short getUnitsInStock() {
		return unitsInStock;
	}

	public void setUnitsInStock(Short unitsInStock) {
		this.unitsInStock = unitsInStock;
	}

	public Short getUnitsOnOrder() {
		return unitsOnOrder;
	}

	public void setUnitsOnOrder(Short unitsOnOrder) {
		this.unitsOnOrder = unitsOnOrder;
	}

	public Short getReorderLevel() {
		return reorderLevel;
	}

	public void setReorderLevel(Short reorderLevel) {
		this.reorderLevel = reorderLevel;
	}

	public boolean isDiscontinued() {
		return discontinued;
	}

	public void setDiscontinued(boolean discontinued) {
		this.discontinued = discontinued;
	}
	
	public Products toProducts(ProductsDTO productsDTO, Products products, Suppliers suppliers, Categories category)
	{	
		if(productsDTO.getProductName() != null)
			products.setProductName(productsDTO.getProductName());		
		products.setSuppliers(suppliers); //TODO
		products.setCategories(category); //TODO		
		if(productsDTO.getQuantityPerUnit() != null)
			products.setQuantityPerUnit(productsDTO.getQuantityPerUnit());		
		if(productsDTO.getUnitPrice() != null)
			products.setUnitPrice(productsDTO.getUnitPrice());
		if(productsDTO.getUnitsInStock() != null)
			products.setUnitsInStock(productsDTO.getUnitsInStock());
		if(productsDTO.getUnitsOnOrder() != null)
			products.setUnitsOnOrder(productsDTO.getUnitsOnOrder());
		if(productsDTO.getReorderLevel() != null)
			products.setReorderLevel(productsDTO.getReorderLevel());
		products.setDiscontinued(productsDTO.isDiscontinued());
			
		return products;
	}
}
